package com.thechalakas.jay.gridview;
/*
 * Created by jay on 16/09/17. 4:39 PM
 * https://www.linkedin.com/in/thesanguinetrainer/
 */

import android.content.Context;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Adapter;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;

public class ImageAdapter extends BaseAdapter
{
    //first the context

    private Context context;

    //set the context
    public ImageAdapter(Context c)
    {
        context = c;
    }

    //the index of all images
    private Integer[] imageIds =
            {
                    R.drawable.sample_2,R.drawable.sample_3,
                    R.drawable.sample_4,R.drawable.sample_5,
                    R.drawable.sample_6,R.drawable.sample_7,
                    R.drawable.sample_0,R.drawable.sample_1,
                    R.drawable.sample_2,R.drawable.sample_3,
                    R.drawable.sample_4,R.drawable.sample_5,
                    R.drawable.sample_6,R.drawable.sample_7,
                    R.drawable.sample_0,R.drawable.sample_1,
                    R.drawable.sample_2,R.drawable.sample_3,
                    R.drawable.sample_4,R.drawable.sample_5,
                    R.drawable.sample_6,R.drawable.sample_7
            };

    //get the count

    public int getCount()
    {
        return imageIds.length;
    }

    //get Item at position
    public Object getItem(int position)
    {
        return null;
    }

    //get Item Id
    public long getItemId(int position)
    {
        return 0;
    }

    //return the view for the selected item
    public View getView(int position, View convertView, ViewGroup parent)
    {
        Log.i("ImageAdapter","getView  Reached");
        //lets get an image view
        ImageView imageView;
        if(convertView == null)   //this means this isnt being recycled from a previous view
        {
            //because this is brand new lets assign some default things
            imageView = new ImageView(context);
            //lets set the usual stuff layout scale and padding
            imageView.setLayoutParams(new GridView.LayoutParams(85,85));
            imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
            imageView.setPadding(8,8,8,8);
        }
        else
        {
            imageView = (ImageView) convertView;
        }

        Log.i("ImageAdapter","getView  Reached Position is - " + position);
        //set the image for this image view we just grabbed
        imageView.setImageResource(imageIds[position]);
        return imageView;
    }

}
