package com.thechalakas.jay.gridview;


/*
 * Created by jay on 16/09/17. 4:17 PM
 * https://www.linkedin.com/in/thesanguinetrainer/
 */


import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.Toast;

import static com.thechalakas.jay.gridview.R.id.gridview;

public class MainActivity extends AppCompatActivity
{

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Log.i("MainActivity","onCreate Reached");

        //Grab the gridview
        final GridView gridView = (GridView) findViewById(R.id.gridview);

        //set an adapter
        gridView.setAdapter((new ImageAdapter(this)));

        //set a listener
        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener()
        {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l)
            {
                Log.i("MainActivity","setOnItemClickListener onItemClick Reached");
                //Log that something was done
                Toast.makeText(MainActivity.this,"" + i,Toast.LENGTH_SHORT).show();
            }
        });

    }

    /*
        GridView gridview = (GridView) findViewById(R.id.gridview);
    gridview.setAdapter(new ImageAdapter(this));

    gridview.setOnItemClickListener(new OnItemClickListener() {
        public void onItemClick(AdapterView<?> parent, View v,
                int position, long id) {
            Toast.makeText(HelloGridView.this, "" + position,
                    Toast.LENGTH_SHORT).show();
        }
    });
     */

    //Grab the gridview


    //set an adapter


    //set a listener
}
